
#include<iostream>
#include<string.h>

using namespace std;

void Q1();
void Q2();
void Q3();

int main(){
	//Q1();
	Q2();
//	Q3();
}

void Q1(){
	//WAP to see if the line is comment
	char inp[100];
	cout<<"Input the sentence: ";
	cin.getline(inp,sizeof(inp));
	bool isComment = false;
	
	for(int i=0;i<strlen(inp);i++){
		if(inp[i] == '/' && inp[i+1] == '/'){
			isComment = true;			
			break;
		}
	}
	
	if(isComment){
		cout<<"It is comment"<<endl;
	}
}

void Q2(){
	//WAP to see if the string accepts ab, a*b+, aab
	string inp;
	bool Case[3];
	for(int i =0;i<3;i++){
		Case[i] = false;
	}
cout<<"The cases that will be checked are 1. ab, 2. a*b+ and 3. aab"<<endl;
	cout<<"Input the expression value: ";
	cin>>inp;
	
	//Case 1 for ab
	if(inp.length()==2){
		if(inp[0] == 'a' && inp[1] == 'b'){
			Case[0] = true;
		}
	}
	
	//Case 2 for a*b+
	bool isCorrect = false;
	bool bStart = false;
	for(int i=0;i<inp.length();i++){
		if(i==0 && (inp[0] =='b' || inp[0]=='a')){
			isCorrect = true;
		}
		if(inp[i] == 'b' && isCorrect){
			bStart = true;
		}
		if(bStart && inp[i]=='a'){
			isCorrect = false;
			break;
		}
	}
	
	if(isCorrect && bStart){
		Case[1] = true;
	}


	//Case 3 for aab
	if(inp.length() == 3){
		if(inp[0] == 'a' && inp[1] == 'a' && inp[2] == 'b'){
			Case[2] = true;
		}
	}
	
	for(int i=0; i<3;i++){
		if(Case[i] == true){
			cout<<"Case "<<i+1<<" is valid"<<endl;
		}
		else{
			cout<<"Case "<<i+1<<" is not valid"<<endl;
		}
	}	
}

void Q3(){
	//WAP to see if the expression is identifier
	string inp;	
	cout<<"Input the identifier you want to use: "<<endl;
	cin>>inp;
	bool isCorrect = true;
	if(inp.length()>0){
		if(inp[0]=='_'|| (int(inp[0])>=65 && int(inp[0]<=90)) || (int(inp[0])>=97 && int(inp[0])<=112)){
			for(int i=1;i<inp.length();i++){
				if((int(inp[i])>=65 && int(inp[i])<=90) || (int(inp[i])>=97 && int(inp[i])<=122) || (int(inp[i])>=48 && int(inp[i])<=57)){
				}
				else{
					isCorrect = false;
				}
			}
		}
		else{
			isCorrect =false;
		}
	}
	else{
		isCorrect =false;
	}
	
	if(isCorrect){
		cout<<"Identifier accepted"<<endl;
	}
	else{
		cout<<"Identifier not accepted"<<endl;
	}
}
